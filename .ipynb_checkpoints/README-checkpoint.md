# NLP102_Pair_Program

**Authors:** Quan and Rodrigo

## Group task
Compare different spaCy language models (including scispaCy) in terms of their performance in analyzing academic texts from directory of source files

## Week 2 demo
- Run scripts available on "evaluating_3.ipynb"
- Code will throw an error if specified library is not available on user's virtual machine; if that's the case, run the following commands on a terminal window to download the necessary packages:
    - Regular spaCy libraries: 
        - Small: "python -m spacy download en_core_web_sm"
        - Medium: "python -m spacy download en_core_web_md"
        - Large: "python -m spacy download en_core_web_lg"
    - scispaCy libraries:
        - Small: "pip install en_core_sci_sm"
        - Medium: "pip install en_core_sci_md"
        - Large: "pip install en_core_sci_lg"
- Specific instructions for each script included inside notebook

## Language Model Outputs From Two Very Similar Documents 
Both grant proposals are "research strategy" files, from nlp101 directory 

LARGE BASE spaCy
0.9795579833914119

MID BASE spaCy
0.9831340765274225

SM BASE spaCy
0.0

LARGE SCI BASE spaCy
0.9409949051875138

MD SCI BASE spaCy
0.9437450515027873

SM SCI BASE spaCy
0.0

SCIBERT
Working on output
